# 03.Git.Hosting

## The Links to repo

[github.com/Logy777/02.Git.Local](https://github.com/Logy777/02.Git.Local)
 
[bitbucket.org/Logy777/02.git.local](https://bitbucket.org/Logy777/02.git.local) 

[gitlab.com/Logy777/02.git.local](https://gitlab.com/Logy777/02.git.local) 

## The named repo list (git remote -v)

```bash
bitbucket	git@bitbucket.org:Logy777/02.git.local.git (fetch)
bitbucket	git@bitbucket.org:Logy777/02.git.local.git (push)
gitlab	git@gitlab.com:Logy777/02.git.local.git (fetch)
gitlab	git@gitlab.com:Logy777/02.git.local.git (push)
origin	git@github.com:Logy777/02.Git.Local.git (fetch)
origin	git@github.com:Logy777/02.Git.Local.git (push)
```

## The pushing script content ( push2all.sh)

```bash
#!/bin/bash
git push --all  bitbucket
git push --all  gitlab
git push --all  origin
echo "pushed to all the repo"
```
