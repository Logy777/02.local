# 02.Git.Local

## Release fase log (all branches)

```bash

* 03ebc90 The critical bug found and FIXED
| * cbd7f81 The FEATURE added in features/do_one
| * 046a2e8 The 2-nd commit in dev. Adding the text line in steps.txt
| * 6091ce7 The 1-st commit in dev. Adding the text line in steps.txt
|/  
* 7d34978 The 2-nd commit in master. Adding the text line in steps.txt
* 1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created

```

## Hotfix deploy & "cherry-pick" the hotfix-commit to the top of all the branches

```bash

* 987fa1c The critical bug found and FIXED
| * b0ff848 The critical bug found and FIXED
|/  
| *   fa662ca Hotfix deploy in master. (hotfix's bug FIXED)
| |\  
|/ /  
| * 03ebc90 The critical bug found and FIXED
* | cbd7f81 The FEATURE added in features/do_one
* | 046a2e8 The 2-nd commit in dev. Adding the text line in steps.txt
* | 6091ce7 The 1-st commit in dev. Adding the text line in steps.txt
|/  
* 7d34978 The 2-nd commit in master. Adding the text line in steps.txt
* 1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created

```

## final master branch log

```bash

fa662ca Hotfix deploy in master. (hotfix's bug FIXED)
03ebc90 The critical bug found and FIXED
cbd7f81 The FEATURE added in features/do_one
046a2e8 The 2-nd commit in dev. Adding the text line in steps.txt
6091ce7 The 1-st commit in dev. Adding the text line in steps.txt
7d34978 The 2-nd commit in master. Adding the text line in steps.txt
1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created
```
## dev branch log

```bash

b0ff848 The critical bug found and FIXED
cbd7f81 The FEATURE added in features/do_one
046a2e8 The 2-nd commit in dev. Adding the text line in steps.txt
6091ce7 The 1-st commit in dev. Adding the text line in steps.txt
7d34978 The 2-nd commit in master. Adding the text line in steps.txt
1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created

```

## features/do_one branch  log

```bash

987fa1c The critical bug found and FIXED
cbd7f81 The FEATURE added in features/do_one
046a2e8 The 2-nd commit in dev. Adding the text line in steps.txt
6091ce7 The 1-st commit in dev. Adding the text line in steps.txt
7d34978 The 2-nd commit in master. Adding the text line in steps.txt
1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created

```

## hotfix/we_gonna_die branch  log

```bash

03ebc90 The critical bug found and FIXED
7d34978 The 2-nd commit in master. Adding the text line in steps.txt
1ab4d5d The 1-st commit in the master. The Repo and the steps.txt created

```

## history list

```bash

 1084  cd sa_local
 1085  git init
 1086  nano .gitignore
 1087  git status
 1088  git add -all
 1089  git add --all
 1090  git branch
 1091  git status
 1092  nano steps.txt
 1093  git add -all
 1094  git add --all
 1095  git commit -m "The 1-st commit in the master. The Repo and the steps.txt created"
 1096  git config --global user.name "Dzianis Zavoiskikh"
 1097  git config --global user.email "6557587@gmail.com"
 1098  nano steps.txt 
 1099  git status
 1100  git commit -a -m "The 2-nd commit in master. Adding the text line in steps.txt"
 1101  git status
 1102  git checkout -b dev
 1103  git branch
 1104  nano steps.txt 
 1105  git commit -a -m "The 1-st commit in dev. Adding the text line in steps.txt"
 1106  nano steps.txt 
 1107  git commit -a -m "The 2-st commit in dev. Adding the text line in steps.txt"
 1108  git status
 1109  git log --oneline
 1110  git commit -ammend
 1111  git commit -amend
 1112  git commit --amend
 1113  git log --oneline
 1114  git checkout -b features/do_one
 1115  git branch
 1116  nano steps.txt 
 1117  git commit -a -m "The FEATURE added in features/do_one"
 1118  git status
 1119  git log
 1120  history 100 >> cmdlist_1
 1121  cat cmdlist_1 |less
 1122  cd ..
 1123  cp sa_local sa_local_copy
 1124  cp -r sa_local sa_local_copy
 1125  ls
 1126  cd sa_local_copy/
 1127  git branch
 1128  git log --online --graph --all
 1129  git log --oneline --graph --all
 1130  git log --oneline --graph
 1131  git checkout master
 1132  git log --oneline --graph
 1133  git log --oneline --graph --all
 1134  git branch
 1135  cd ..
 1136  rm -r sa_local_copy
 1137  y
 1138  ls -la
 1139  cd sa_local/
 1140  git status
 1141  rm cmdlist_1 temp/
 1142  ls
 1143  ls -la
 1144  mkdir tmp
 1145  ls -la
 1146  rm cmdlist_1 tmp/
 1147  git ranches
 1148  git branch
 1149  git log
 1150  git log --oneline 
 1151  cd sa
 1152  cd ..
 1153  cp -r sa_local sa_local_copy
 1154  cd sa_local_copy/
 1155  git branch
 1156  git checkout master
 1157  git logout
 1158  git log --oneline
 1159  git checkout -b hotfix/we_gonna_die
 1160  nano steps.txt 
 1161  git log --oneline
 1162  git log --oneline --graph
 1163  git status
 1164  git commit -a -m "The critical bug found and FIXED"
 1165  git status
 1166  git log --oneline --graph
 1167  git log --oneline --graph --all
 1168  mkdir tmp
 1169  git log --oneline --graph --all >> tmp/graph_before_release
 1170  git status
 1171  cat tmp/graph_before_release 
 1172  cd ..
 1173  cp -r sa_local_copy sa_local_copy2
 1174  cd sa_local_copy2
 1175  git branch
 1176  git log --oneline --graph --all
 1177  git status
 1178  git checkout dev
 1179  git merge hotfix/we_gonna_die 
 1180  git rebase hotfix/we_gonna_die 
 1181  git status
 1182  git merge --abort
 1183  git status
 1184  git rebase hotfix/we_gonna_die 
 1185  git am --show-current-patch
 1186  git branch
 1187  git rebase --continue
 1188  git add
 1189  nano steps.txt 
 1190  git rebase --continue
 1191  git status
 1192  git log --oneline
 1193  git rebase --continue
 1194  git add steps.txt 
 1195  git rebase --continue
 1196  nano steps.txt 
 1197  git rebase --abort
 1198  git status
 1199  git log --oneline 
 1200  git branch
 1201  git merge features/do_one 
 1202  git status
 1203  git log --oneline 
 1204  cat steps.txt 
 1205  git checkout master 
 1206  git merge dev 
 1207  git log --oneline --graph
 1208  cat steps.txt 
 1209  git log --oneline --graph >> tmp/Release_Graph_(master)
 1210  git log --oneline --graph >> tmp/Release_Graph_master
 1211  ls -la tmp/
 1212  git branch
 1213  cat steps.txt 
 1214  git branch
 1215  git status
 1216  git log --oneline --graph
 1217  git log --oneline --graph --all
 1218  git log --oneline --graph --all >> tmp/Release_Graph_master_
 1219  cat tmp/Release_Graph_master_
 1220  cat tmp/Release_Graph_master
 1221  cd ..
 1222  cp -r sa_local_copy2 sa_local_copy3
 1223  cd sa_local_copy3
 1224  cat tmp/Release_Graph_master
 1225  cat tmp/Release_Graph_master_
 1226  git branch
 1227  git log --oneline
 1228  cat steps.txt 
 1229  git merge hotfix/we_gonna_die 
 1230  nano steps.txt 
 1231  git status
 1232  git commit -a -m "Hotfix deploy in master. (hotfix's bug FIXED)"
 1233  git log -oneline
 1234  git log --oneline
 1235  cat steps.txt 
 1236  history | grep shut
 1237  history | grep shutdown
 1238  sudo poweroff
 1239  ls -la
 1240  vim .bashrc 
 1241  vi .bashrc 
 1242  vitutor
 1243  nano .bashrc 
 1244  cd sa/
 1245  ls
 1246  cd sa_local_copy3
 1247  ls -la
 1248  ls tmp/
 1249  cat tmp/Release_Graph_master
 1250  cat tmp/Release_Graph_master_
 1251  cd ..
 1252  cd sa_local_copy2
 1253  ls tmp/
 1254  cat tmp/Release_Graph_master_
 1255  cat tmp/Release_Graph_master
 1256  git log --oneline
 1257  cd ../sa_local_copy3
 1258  git log --oneline
 1259  git log --oneline -graph
 1260  git log --oneline --graph --all
 1261  git branch
 1262  git checkout hotfix/we_gonna_die 
 1263  git log --oneline
 1264  git checkout dev 
 1265  git log --oneline
 1266  git cherry-pick 03ebc90
 1267  cat steps.txt 
 1268  nano steps.txt 
 1269  git branch
 1270  git status
 1271  git add --all
 1272  git status
 1273  git cherry-pick --continue 
 1274  git status
 1275  git log --oneline
 1276  git checkout features/do_one  
 1277  git log --oneline
 1278  git cherry-pick 03ebc90
 1279  nano steps.txt 
 1280  git add --all
 1281  git cherry-pick --continue 
 1282  git status
 1283  git log --oneline
 1284  git --oneline --graph --all
 1285  git log --oneline --graph --all
 1286  git checkout master
 1287  git log --oneline --graph --all

```

## steps.txt content (the one-file product)


Local repo created. Added the '.gitignore'. This steps.txt created with this line of text.

After adding that the 2-nd commit in master will be generated.

dev-branch: this line added as a 1-st commit in dev.

dev-branch: this line added as a 2-nd commit in dev.

This is a FEATURE added in feature/do_one

A critical bug found and FIXED in hotfix
