# 07.Kubernetes

## The minikube dashboard screen snapshot
![alt text](https://github.com/Logy777/02.Git.Local/blob/master/07.Kubernetes/dashboard.png "The minikube dashboard screen snapshot")

## history (minikube)
```bash  
 1981  vboxmanage --version
 1982  grep -E --color 'vmx|svm' /proc/cpuinfo
 1983  sudo apt-get update && sudo apt-get install -y apt-transport-https
 1984  curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
 1985  echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
 1986  sudo apt-get update
 1987  sudo apt-get install -y kubectl
 1988  kubectl version
 1989  curl -Lo minikube https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64   && chmod +x minikube
 1990  ls -la
 1991  ls -la | less
 1992  echo $PATH
 1993  sudo mkdir -p /usr/local/bin/
 1994  sudo install minikube /usr/local/bin/
 1995  ls -la | less
 1996  sudo ls -la /usr/local/bin/
 1997  rm minikube 
 1998  ls -la
 1999  ls -la | less
 2000  minikube start --help
 2001  minikube start
 2002  free
 2003  free -h
 2004  kubectl get nodes
 2005  ls
 2006  ls -la
 2007  ls -la ~/.minikube/
 2008  kubectl claster-info
 2009  kubectl cluster-info
 2010  ls -la ~/
 2011  nano  ~/.kube/config 
 2012  minikube service list
 2013  kubectl get services
 2014  minikube plugin list
 2015  minikube addons list
 2016  minikube addons dashboard enable
 2017  minikube addons enable dashboard
 2018  kubectl proxy --address='0.0.0.0' --disable-filter=true &
 2019  minikube addons list
 2020  minikube stop
 2021  ps aux | grep 0.0.0.0
 2022  kill -9 8234
 2023  ps aux | grep 0.0.0.0
 2024  minikube delete
 2025  minikube start --cpus=2 --memory='2g'
 2026  history 20
 2027  minikube addons list
 2029  minikube addons list
 2030  minikube addons enable ingress
 2031  minikube addons list
 2032  history 30
 2033  kubectl proxy --address='0.0.0.0' --disable-filter=true &
 2035  history 100  >> 07.Kubernetes.history
```
